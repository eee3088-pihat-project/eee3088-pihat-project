EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title "Motor Position feedback Opamp "
Date "2021-06-03"
Rev "0.1"
Comp "EEE3088-Group45"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 2200 2850 0    50   Input ~ 0
12Vcc
Text GLabel 2200 3550 0    50   Input ~ 0
5Vcc
Text GLabel 2100 4050 0    50   Input ~ 0
GND
$EndSCHEMATC
