EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title "Motor Position Feedback"
Date "2021-06-03"
Rev "1.0"
Comp "EEE3088-Group45"
Comment1 "Designing position feedback circuitry "
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Sensor_Magnetic:A1302KLHLT-T U2
U 1 1 60B3D0DC
P 2450 2850
F 0 "U2" H 2221 2896 50  0000 R CNN
F 1 "A1302KLHLT-T" H 2221 2805 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23W" H 2450 2500 50  0001 L CIN
F 3 "http://www.allegromicro.com/~/media/Files/Datasheets/A1301-2-Datasheet.ashx" H 2350 2850 50  0001 C CNN
	1    2450 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R1
U 1 1 60B40979
P 3200 2600
F 0 "R1" H 3268 2646 50  0000 L CNN
F 1 "100k" H 3268 2555 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 3240 2590 50  0001 C CNN
F 3 "~" H 3200 2600 50  0001 C CNN
	1    3200 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R2
U 1 1 60B41148
P 3450 3100
F 0 "R2" V 3245 3100 50  0000 C CNN
F 1 "150" V 3336 3100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 3490 3090 50  0001 C CNN
F 3 "~" H 3450 3100 50  0001 C CNN
	1    3450 3100
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R3
U 1 1 60B418DC
P 3950 2600
F 0 "R3" H 4018 2646 50  0000 L CNN
F 1 "10k" H 4018 2555 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 3990 2590 50  0001 C CNN
F 3 "~" H 3950 2600 50  0001 C CNN
	1    3950 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R4
U 1 1 60B42133
P 3950 3400
F 0 "R4" H 4018 3446 50  0000 L CNN
F 1 "200" H 4018 3355 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 3990 3390 50  0001 C CNN
F 3 "~" H 3950 3400 50  0001 C CNN
	1    3950 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 2850 3200 2850
Wire Wire Line
	3200 2850 3200 3100
Wire Wire Line
	3200 3100 3300 3100
Wire Wire Line
	3600 3100 4400 3100
Wire Wire Line
	3950 2300 3950 2450
Wire Wire Line
	3950 2750 3950 3000
Wire Wire Line
	3950 3550 3950 3600
Wire Wire Line
	3200 2300 3200 2450
Wire Wire Line
	3200 2750 3200 2850
Connection ~ 3200 2850
Wire Wire Line
	4400 3000 3950 3000
Connection ~ 3950 3000
Wire Wire Line
	3950 3000 3950 3250
Wire Wire Line
	4400 2900 4250 2900
Wire Wire Line
	5600 2900 5600 2300
Wire Wire Line
	5600 2300 3950 2300
Text GLabel 2350 2250 1    50   Input ~ 0
+5Vcc
Wire Wire Line
	2350 3250 2350 3600
Wire Wire Line
	2350 3600 3950 3600
Connection ~ 3950 3600
$Comp
L Device:R_US R6
U 1 1 60B4DA29
P 5950 3050
F 0 "R6" H 6018 3096 50  0000 L CNN
F 1 "2.7K" H 6018 3005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5990 3040 50  0001 C CNN
F 3 "~" H 5950 3050 50  0001 C CNN
	1    5950 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R5
U 1 1 60B4F5F3
P 5050 2550
F 0 "R5" V 4845 2550 50  0000 C CNN
F 1 "7.5K" V 4936 2550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5090 2540 50  0001 C CNN
F 3 "~" H 5050 2550 50  0001 C CNN
	1    5050 2550
	0    1    1    0   
$EndComp
Wire Wire Line
	4250 2550 4250 2900
Wire Wire Line
	4250 2550 4900 2550
Wire Wire Line
	5200 2550 5950 2550
Wire Wire Line
	5950 2550 5950 2900
Wire Wire Line
	5950 3600 3950 3600
Wire Wire Line
	5950 3200 5950 3600
Wire Wire Line
	5950 2550 6150 2550
Connection ~ 5950 2550
Text HLabel 6150 2550 2    50   Output ~ 0
Vpout
Wire Wire Line
	2350 2250 2350 2300
Wire Wire Line
	3200 2300 2350 2300
Connection ~ 2350 2300
Wire Wire Line
	2350 2300 2350 2450
Wire Wire Line
	3950 2300 3950 2200
Connection ~ 3950 2300
Text GLabel 3950 2200 1    50   Input ~ 0
+12Vcc
$Comp
L power:-12V #PWR0108
U 1 1 60BB19B6
P 4300 3300
F 0 "#PWR0108" H 4300 3400 50  0001 C CNN
F 1 "-12V" H 4315 3473 50  0000 C CNN
F 2 "" H 4300 3300 50  0001 C CNN
F 3 "" H 4300 3300 50  0001 C CNN
	1    4300 3300
	-1   0    0    1   
$EndComp
Text GLabel 3950 3600 3    50   Input ~ 0
GND
Wire Wire Line
	4400 3200 4300 3200
Wire Wire Line
	4300 3200 4300 3300
NoConn ~ 5500 3200
NoConn ~ 5500 3100
NoConn ~ 5500 3000
Wire Wire Line
	5500 2900 5600 2900
$Comp
L LM358P:LM358P IC1
U 1 1 60B3EE0C
P 4400 2900
F 0 "IC1" H 4950 3165 50  0000 C CNN
F 1 "LM358P" H 4950 3074 50  0000 C CNN
F 2 "DIP794W53P254L959H508Q8N" H 5350 3000 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm358a.pdf" H 5350 2900 50  0001 L CNN
F 4 "LM358P, Dual Operational Amplifier 0.7MHz 5 to 28V, 8-Pin PDIP" H 5350 2800 50  0001 L CNN "Description"
F 5 "5.08" H 5350 2700 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 5350 2600 50  0001 L CNN "Manufacturer_Name"
F 7 "LM358P" H 5350 2500 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "595-LM358P" H 5350 2400 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/LM358P?qs=X1HXWTtiZ0QtOTT8%252BVnsyw%3D%3D" H 5350 2300 50  0001 L CNN "Mouser Price/Stock"
F 10 "LM358P" H 5350 2200 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/lm358p/texas-instruments" H 5350 2100 50  0001 L CNN "Arrow Price/Stock"
	1    4400 2900
	1    0    0    -1  
$EndComp
$EndSCHEMATC
