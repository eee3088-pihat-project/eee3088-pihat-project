# Getting started with using the motor position feedback HAT
In this guide, we will demonstrate how the user could use the microHAT in the context of contolling dc motors.

## Powering it up
Wire up your battery pack to the Power terminal block on the connectors of the HAT. The HAT is polarity protected but still it's a good idea to check your wire polarity. Once the HAT has the correct polarity, you'll see the green LED light up.

Please note the HAT does not power the Raspberry Pi, and we strongly recommend having two seperate power supplies - one for the Pi and one for the motors, as motors can put a lot of noise onto a power supply and it could cause stability problems!

## Enable IC2
The DC and Stepper Motor uses I2C to communicate with your Raspberry Pi.

## Python Installation of MotorKit library
You'll need to install the Adafruit_Blinka library that provides the CircuitPython support in Python. This will require enabling I2C on your Raspberry Pi and verifying you are running Python 3.

## Using DC Motors 
DC motors are used for all sort of robotic projects. The Motor HAT can drive up to 2 DC motors bi-directionally. That means they can be driven forwards and backwards.The speed can also be varied at 0.5% increments using the high-quality built in PWM. This means the speed is very smooth and won't vary.

## Connecting DC Motors
To connect a motor, simply solder two wires to the terminals on the motor (if they're not already there!) and then connect them to driver pin connectors on the Pi hat. If your motor is running 'backwards' from the way you expect, swap the wires in the terminal block.

## Using stepper motor
Stepper motors are great for (semi-)precise control, perfect for many robot and CNC projects. This HAT supports up to 2 stepper motors. The python library works identically for bi-polar and uni-polar motors.

## Connecting Stepper Motors
For unipolar motors: to connect up the stepper, first figure out which pins connected to which coil, and which pins are the center taps. If its a 5-wire motor then there will be 1 that is the center tap for both coils. The center taps should both be connected together to the center GND terminal on the Motor HAT output block, then coil 1 should connect to one one motor driver say pin 3(1Y) and 6(2Y) and coil 2 should connect to the other pins say 11(3Y) and 14(4Y).

For bipolar motors: its just like unipolar motors except there's no 5th wire to connect to ground.


