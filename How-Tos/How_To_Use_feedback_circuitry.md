# How to use motor position feedback circuitry to control dc motors.  

## Introduction
The motor position feedback circuitry is controller that uses that L293D motor and opAmp LM358P which uses an H-Bridge to easily control the direction and speed and detects the position stepper motor. This tutorial will show you how to use it.
The entire system is connected across the terminals of the motor that comes with Hall effect magnetic sensor.

### Text instructions
* The motor driver is controlled at the pins of the L293D control the speed and direction of the motors.
* GPIO17_GEN0, GPIO27_GEN2, GPIO22_GEN3 and GPIO18_GEN control the direction of the stepper motor connected to pin 3(1Y), 6(2Y), 11(3Y) and pin 14(4Y) of L293D.
* You can power the L298N with up to 12V by plugging your power source into the pin on the L298N labelled "12V". The pin labelled "5V" is a 5V output that you can use to power your motor HAT.
* You can change the speed with the EN pins. Both pin 1(EN1,2) and pin 9(EN3,4) controls the speed of the stepper motor.