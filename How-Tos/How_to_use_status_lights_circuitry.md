# How to use status lights circuitry

## Introduction
This module of the hat would have LEDS that indicate when the motor is ON or OFF also they will show when it is drawing too much current.A digital potentiometer can be used to vary sensitivity of the circuitry.

### Text instructions
* connect the potentiometer to the GPIO2 SDA, GPIO3 SCL1 pins
* connect the circuitry to the motor

