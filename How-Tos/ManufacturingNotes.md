# How to build the motor position feedback HAT
## Overview

In this guide, we will build a simple motor microHAT server project on a Raspberry Pi zero. The Raspberry Pi uses a fully-dedicated PWM driver chip onboard to both control motor direction and speed. This chip handles the motor and speed controls over I2C. Only two GPIO pins (SDA & SCL) are required to drive the multiple motors, and since it's I2C you can also connect any other I2C devices or HATs to the same pins.

Once your microHAT includes these connections, your microHAT will be able to check the different states of the circuit submodules.

# What you will need
At its most basic, the process of manufacturing this HAT consists on the following specifications:
* 40W Pin compliant Header
* Power switching regulator (PSU) delivering 9.5V to widget, 5V to back-powering GPIO pins, 3.3V leds status and op-amp.
* External Power source connector and additional connector for motors
* An input ZVD circuit
* Overload protection FET circuit with status leds
* LD392 Motor Driver chipset driving stepper motor
* Up to 2 stepper motors (unipolar or bipolar) with single coil, double coil, interleaved or micro-stepping.
* Big terminal block connectors to easily hook up wires (18-26AWG) and power
* Polarity protected 2-pin terminal block and jumper to connect external 5-12VDC power
* Install the easy-to-use Python library.

# MicroHAT PCB setup
If you dont have kicad software or tips for creation of PCB, find the instructions and download the kicad installer [here](https://www.kicad.org/download/windows/).
To meet the standard version of this microHAT, go through the footprints and layout of the pcb in this repo.





